<?php
function brainstorm_update_settings() {
  $form = array();
  $form['brainstorm_update_check_frequency'] = array(
    '#type' => 'radios',
    '#title' => t('Check for updates'),
    '#default_value' => variable_get('brainstorm_update_check_frequency', 1),
    '#options' => array(
	  '0' => t('Never'),
      '1' => t('Daily'),
      '7' => t('Weekly'),
    ),
    '#description' => t('Select how frequently you want to automatically check for new releases of brainstormblogger system'),
  );
  $form = system_settings_form($form);
  // Custom valiation callback for the email notification setting.
  $form['#validate'][] = 'brainstorm_update_settings_validate';
  return $form;
}
function brainstorm_update_settings_validate($form, &$form_state) {
}


