<?php

function brainstorm_manual_status() {
  if (_brainstorm_update_refresh()) {
    drupal_set_message(t('Attempted to fetch information about all available new releases of Brainstormblogger distribution.'));
  }
  else {
    drupal_set_message(t('Unable to fetch any information about available new releases of Brainstormblogger distribution.'), 'error');
  }
  drupal_goto('admin/reports/status');
}
function _brainstorm_update_refresh() {
  static $fail = array();
  global $base_url;
  $fetch_url = 'http://brainstormblogger.org/releases/release1.php?url=' 
    . drupal_urlencode(url(NULL, array('absolute' => TRUE, ) ) ) 
    . '&mail=' . drupal_urlencode(variable_get('site_mail', '') );
  $data = drupal_http_request($fetch_url);
  if($data->code != '200'){
	watchdog('brainstorm_update', 'Unable to fetch any information about available new releases and updates.', array(), WATCHDOG_ERROR, l(t('view'), 'admin/reports/updates'));  
	return FALSE;
  }
  $frequency = variable_get('brainstorm_update_check_frequency', 1);
  if(!$frequency ) $frequency = 1;
  $frequency = variable_get('update_check_frequency', 1);
  $available = (int) $data->data;
  _brainstorm_update_cache_set('brainstorm_update_available_releases', $available, time() + (86400 * $frequency));
  watchdog('brainstorm_update', 'Attempted to fetch information about all available new releases and updates.', array(), WATCHDOG_NOTICE, l(t('view'), 'admin/reports/updates'));
}
